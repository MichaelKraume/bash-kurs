[[ $# -eq 2 ]] && [[ -f $1 ]] || {
	echo Eine Fehlermeldung 1>&2
	echo "Bitte gib genau 2 argumente an, von denen das erste eine existierend Datei ist"
	exit 1
}
[[ ! -d $2 ]] && mkdir -p $2
echo $2 | xargs -L 1 wget -P $2
