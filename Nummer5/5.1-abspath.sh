if [[ ! $@ ]]; then
	echo $PWD
fi
for param in $@; do
	if [[ -f $param ]]; then
		echo $(realpath $param)
	fi
done
