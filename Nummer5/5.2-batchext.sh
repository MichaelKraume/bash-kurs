if [[ $# -le 1 ]]; then
	echo "bitte mindestens 2 Argumente mitgeben"
	exit 1
fi
	[[ $1 == '.'* ]]&&ext=$1##'.'
	[[ $@ == *"--no-nonsense"* ]]&&ext=$1##*'.' #falls $1 zum beispiel .java.html.css.jpeg.evenmorejpeg oder einfach nur .wav statt wav ist
	if [[ $@ == *"--No-nonsense"* ]]; then
		ext=${1#*'.'} #falls $1 zum beispiel .java.html.css.jpeg.evenmorejpeg oder einfach nur .wav statt wav ist
		ext=${ext%%.*}
	fi
for param in ${@:2}; do
	if [[ -f $param ]]; then
		mv $param $(basename $param).$ext
	fi
done

