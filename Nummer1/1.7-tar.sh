#!/bin/bash

echo "(a)"
tar -cvf mytxts.tar *.txt

echo "(b)"
tar --list mytxts.tar

echo "(c)"
tar --list mytxts.tar | sort > mytxts_names
