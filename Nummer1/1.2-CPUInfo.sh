#!/bin/bash

echo "(a)"
cores=$(grep "cpu cores" /proc/cpuinfo)
cores=${var##*\s:}
echo $cores

echo "(b)"
sed -n '5,12p' /proc/cpuinfo
#alternativ mit mitteln der 1. VL
head -n12 /proc/cpuinfo | tail -n+5

echo "(c)"
grep -A 1 -B 1 "cpu MHz" /proc/cpuinfo
