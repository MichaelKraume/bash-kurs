function usage ()
{
	echo "Usage :  $0 [options] [--]

    Options:
    -h|--help       Display this message"
}
LONGOPTS="help"
SHORTOPTS="h"
options=$(getopt -l "$LONGOPTS" -- "$SHORTOPTS" "$@")
[ $? -eq 0 ] || { 
    echo "Incorrect options provided"
    exit 1
}
eval set -- "$options"
#echo $@
while [[ ! $1 == "--" ]]; do
	case "$1" in
		-h|--help )
			usage
			;;
	esac
	shift
done
