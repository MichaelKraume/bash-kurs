#!/bin/bash

printenv > before
cd -
printenv > $OLDPWD/after
cd -
vim -d before after
#man sollte erkennen, dass PWD und OLDPWD getauscht sind
shred -u before after
