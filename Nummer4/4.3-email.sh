read email
#email='max.mustermann@uni-jena.de'
prefix='[a-zA-Z]+[a-zA-Z._%0-9]*'
host='[a-zA-Z]+([a-zA-Z]|-)*[a-zA-Z0-9]+'
suffix='[a-zA-Z]{1,3}'
mailregex=$prefix@$host'\.'$suffix
if [[ $email =~ ^$mailregex ]]; then
	echo E-mail is valid
	exit 0
else
	echo E-mail is invalid
	exit 0
fi
exit 1
