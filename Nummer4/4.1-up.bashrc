#!/bin/bash
# vi: ft=sh

function up() {
	echo $1
	k=${1:-1}
	echo $k
	for (( i = 0; i < $((k)); i++ )); do
		cd ../
		echo $PWD
	done
}
#up $@

##up muss als funktion implementiert werden, da skripte sich eine eigene umgebung aufmachen, in denen lustig rum cden und die umgebung mit entsprechendem PWD dann wieder schließen.
##um die funktion in allen neuen shells zur verfügung stehen zu haben, muss man die funktion in die bashrc schreiben (oder mit source arbeiten)
