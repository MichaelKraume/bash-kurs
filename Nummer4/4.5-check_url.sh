#!/bin/bash


prefix='(?:https?://)?(?:www.)?'
body='[a-zA-Z.-]+'
ext='\.[a-zA-Z]{1,3}'
suffix='[a-zA-Z/0-9.]*'

echo "$1" | grep -E "$prefix$body$ext$suffix"
