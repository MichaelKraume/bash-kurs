tel='^[0-9]{5,}$'
vorwahl='0[0-9]{2,4}'
trennzeichen='[/-]?'
rufnummer='[0-9]{4,8}'
vorwahl2='[0+][0-9]{2,4}'
tel="$vorwahl2$trennzeichen$rufnummer"
echo $1 | grep -E "$tel"
