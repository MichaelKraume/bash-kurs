#!/bin/bash

for entry in "$@"; do
	[[ -e $entry ]]&&echo $entry existiert
	[[ -d $entry ]]&&echo $entry ist Ordner
	[[ -f $entry ]]&&echo $entry ist Datei
done
