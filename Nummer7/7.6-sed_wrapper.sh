function usage ()
{
	echo "Usage :  $0 [options] [--]

    Options:
    -g    set the global flag"
}
SHORTOPTS="g"
while getopts "$SHORTOPTS" opt; do
	case $opt in
		-g )
			global=true
			;;
	esac
done

[[ $# -eq 3 ]] || {
	usage
	exit 1
}
if [[ $global == true ]]; then
	sed 's/'"$1"'/'"$2"'/g' $3
else
	sed 's/'"$1"'/'"$2"'/' $3
fi
