lines=10
#process options
SHORTOPTS="n:"
while getopts "$SHORTOPTS" opt; do
	case $opt in
		n )
			lines=$OPTARG
			;;
	esac
done
shift $((OPTIND-1))
[[ $# -ge 1 ]] || {
	echo "Keine argumente übergeben"
	exit 1
}
for file in $@; do
	if [[ -f $file ]]; then
		sed -nr "1,$lines p" $file
	else
		echo "$1 ist keine datei. überspringe..."
	fi
done
