lines=10
#process options
SHORTOPTS="n:"
while getopts "$SHORTOPTS" opt; do
	case $opt in
		n )
			lines=$OPTARG
			;;
	esac
done
shift $((OPTIND-1))
[[ $# -ge 1 ]] || {
	echo "Keine argumente übergeben"
	exit 1
}
for file in $@; do
	if [[ -f $file ]]; then
		# einfache lösung: tac $file | head | tac
		# es soll nur mit sed gearbeitet werden
		# head wurde bereits in der vorigen aufgabe gemacht. also emuliere ich tac mit sed.
		#sed '1!G' gibt ab der 2. zeile die zeile aus und hängt den hold space an.
		#sed '1!G;h' gibt die zeile aus, hängt den hold space an und schreibt alles was es gerade ausgegeben hat in den Hold space
		#sed -n '1!G;h;$p' gleiches wie vorher, gibt nur die letzte Ausgabe an den stdoutput
		#output: letzte zeile+holdspace
		# 	holdspace = vorletzte zeile + vorheriger holdspace
		# 	vorheriger holdspace = vorvorletzte[...]
		##hiermit haben wir tac emuliert.
		sed '1!G;h;$p' $file | sed -nr "1,$lines p" | sed '1!G;h;$p'
	else
		echo "$1 ist keine datei. überspringe..."
	fi
done
