#!/usr/bin/env bash

function usage ()
{
	echo "Usage :  $0 marker-file marked_sequence-file"
}
#check if both are valid files
[[ $# -eq 2 ]] || {
	usage
	exit 1
}
# ablage dateien erstellen
markers=( $(cat $1) )
for marker in ${markers[@]}; do
	touch $marker
done
touch UNMARKED
#zeilen einsortieren
while read line; do
	for marker in ${markers[@]}; do
		if [[ $line == $marker\ * ]]; then
			echo $line | sed 's/'"$marker"'\s//' >> $marker
			continue 2
		fi
	done
	echo $line >> UNMARKED
done < $2
#shred -u UNMARKED ${markers[@]}
