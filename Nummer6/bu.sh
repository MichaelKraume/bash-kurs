function usage ()
{
	echo "Usage :   [options] [--]

    Options:
    -h|--help       Display this message
    -c|--compress   compress your file"
}
LONGOPTS="help,compress"
SHORTOPTS="hc"
options=$(getopt -l "$LONGOPTS" -- "$SHORTOPTS" "$@")
[ $? -eq 0 ] || { 
    echo "Incorrect options provided"
    exit 1
}
eval set -- "$options"
while [[ $1  != "--" ]]; do
	case $1 in
		-h|--help )
			usage
			exit 0
			;;
		-c|--compress )
			compress=true
			;;
	esac
	shift
done; shift
if [[ $# -eq 0 ]]; then
	echo "bitte gib eine Datei zum backupen an"
	exit 1
fi
for file in $@; do
	if [[  -f $file ]]; then
		if [[ $compress == true ]]; then
			gzip -S ".gz.bu" $file
			chmod -x -w $file.gz.bu
		else
			cp -i $file $file.bu # -i um bei überschreiben nachzufragen. evtl basename weglassen
			chmod -x -w $file.bu
		fi

	else
		echo "$file existiert nicht. überspringe..."
	fi
done

exit 0
##
